Gerne kann zum Open Source-Code beigetragen werden.

Vorschläge zu Fehlerbehebungen und neuen Features mit Quellcode können als Merge Request gestellt werden:

*   Klonen Sie das Repository damit Sie lokal arbeiten können --- `git clone https://gitlab.com/LeaSat/open-access-reporting.git`
*   Erstellen Sie einen Branch --- `git checkout -b new-feature`
*   Nehmen Sie Ihre Anpassungen vor. Achten Sie hierbei auf den `_site`-Ordner: Bitte verschieben Sie den HTML-Output nach `public`, bevor Sie einen Commit erstellen.
*   Übertragen Sie Ihre Anpassungen --- `git commit -m "added new feature"`
*   Pushen Sie den Branch --- `git push --set-upstream origin new-feature`
*   Stellen Sie einen Merge Request

Gerne kann auch ein Issue mit einem der folgenden Label eröffnet werden:

*   `bug` --- Fehlerbehebungen
*   `discussion` --- Diskussion
*   `documentation` --- Dokumentation
*   `enhancement` --- Verbesserungen und neue Features
*   `suggestion` --- Vorschläge für weitere Funktionalitäten
*   `support` --- Anfragen zu Support
