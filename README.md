# Open-Access-Reporting

Die Skripte ermöglichen ein automatisiertes Reporting über veröffentlichte Open-Access-Artikel und verausgabte Mittel im Rahmen von Open-Access-Publikationsfonds sowie Transformationsverträgen.

Grafisch ausgewertet werden für Gold und Hybrid Open Access:

*   die Entwicklung der Publikationszahlen über die Jahre,
*   das Publikationsaufkommen und die Kosten pro Jahr auf Fakultäts- und Institutsebene,
*   die Publikationsanteile der Fakultäten,
*   die Entwicklung der durchschnittlichen APC,
*   die Verausgabung von Mitteln aus zur Verfügung stehenden Fördertöpfen,
*   die Verlage und Zeitschriften, in denen Forschende bevorzugt publizieren, sowie
*   die publikationsstärksten Institute.

Ergänzt wird die Auswertung um eine durchsuchbare Übersicht der Rohdaten.

# Dokumentation

Fingierte Beispieldaten, auf denen die präsentierten Auswertungen basieren, sind im Ordner `src/data` im Repository verfügbar.

## Datenerhebung und -grundlage

Die Auswertungen basieren auf CSV-Dateien als Auflistung der einzelnen Open-Access-Artikel. Diese Daten können aus Förderanträgen im Rahmen des Publikationsfonds, aus den Dashboards und Reports der Anbieter sowie aus Rechnungen, die Listen veröffentlichter Artikel beinhalten, stammen.

## Datenschema

Die Auswertung basiert auf dem [openCost-Metadatenschema](https://github.com/opencost-de/opencost/tree/main/doc). Angaben zu den einzelnen Open-Access-Artikeln sind in CSV-Dateien hinterlegt (vgl. Dateien im Ordner `src/data` im GitLab-Repository). Für eine grafische Auswertung sind gewisse Variablen verpflichtend zu befüllen.

| Variable | Pflichtfeld | Beschreibung | Entsprechung im openCost-Metadatenschema |
| :--- | ----------: | -----------: | -----------: |
| DOI | nein | DOI der Veröffentlichung | 2.1: doi |
| yearInvoice | ja | Haushaltsjahr | 7.2.3.2: paid |
| yearPublication | nein | Publikationsjahr | - |
| corrAut | ja | *corresponding author* | - |
| faculty | ja | Fakultät | - |
| institute | ja | Institut | - |
| title | nein | Titel der Veröffentlichung | 2.2.1: Title |
| isPartOf | ja | übergeordnete Einheit | 2.2.3: isPartOf |
| publisher | ja | Verlag | 2.2.2: Publisher |
| publicationType | ja | Publikationsform ([COAR-normiertes Vokabular](https://vocabularies.coar-repositories.org/resource_types/)) | 5: publication_type |
| license | ja | Lizenz | - |
| invoiceNumber | nein | Rechnungsnummer | 7.2.1: invoice_number |
| amount | ja | Rechnungsbetrag in Originalwährung | 7.2.4.1: amount |
| currency | ja | Originalwährung ([ISO 4217](https://en.wikipedia.org/wiki/ISO_4217)) | 7.2.4.2: currency |
| amountNet | ja | gezahlter Gesamtbetrag Netto | - |
| tax | ja | Steuersatz | - |
| amountGross | ja | gezahlter Gesamtbetrag Brutto | - |
| amountPaid | ja | gezahlter Betrag (ggf. anteilig) Brutto | 7.2.5.1.1: amount |
| currencyPaid | ja | Währung des gezahlten Betrags ([ISO 4217](https://en.wikipedia.org/wiki/ISO_4217)) | 7.2.5.1.2: currency |
| funding | ja | Fördertopf | - |
| costType | ja | Gebührenart ([openCost-Metadatenschema](https://github.com/opencost-de/opencost/tree/main/doc)) | 7.2.5.1.3: cost_type |
| contract | ja | Zuordnung zu einer Vereinbarung (Bezeichnung) | 7.1.1.2: value |
| contractID | ja | Zuordnung zu einer Vereinbarung ([ESAC-ID](https://esac-initiative.org/about/transformative-agreements/agreement-registry/)) | 7.1.1.1: type |

## Datenbereinigung

Um eine saubere Darstellung der Daten sicherzustellen, wurden die zusammengetragenen Daten bereinigt und z.B. Bezeichnungen der Verlage und Institute normiert. Zur Bereinigung wurde auf [OpenRefine](https://openrefine.org/) (v. 3.5.2) zurückgegriffen .

## Programmierung der Skripte

Die Auswertung und Präsentation der Daten sowie Erstellung der Webseite wurden mit [R](https://cran.r-project.org/) (v 4.2.0) sowie der IDE [RStudio](https://www.rstudio.com/) (v. 2022.02.2+485) vorgenommen.

Folgende R Pakete wurden in den Skripten genutzt und eingebunden:

*   [tidyverse](https://cran.r-project.org/web/packages/tidyverse/index.html) (v. 1.3.1): ein Set von kompatiblen Paketen, unter anderem zur Datenbereinigung und -transformation
*   [highcharter](https://cran.r-project.org/web/packages/highcharter/index.html) (v. 0.9.4): ein R-Wrapper für die JavaScript-Bibliothek Highcharts zur Erstellung interaktiver HTML-Grafiken
*   [htmltools](https://cran.r-project.org/web/packages/htmltools/index.html) (v. 0.5.7): ein Paket zur Erzeugung und Ausgabe von HTML-Dokumenten
*   [DT](https://cran.r-project.org/web/packages/DT/index.html) (v. 0.23): ein Paket zur Erstellung interaktiver HTML-Tabellen basierend auf der JavaScript-Bibliothek DataTables

Über [rmarkdown](https://cran.r-project.org/web/packages/rmarkdown/index.html) (v. 2.14) wurde eine R Markdown Webseite zur Präsentation der Auswertungen erstellt.

## Lokale Nachnutzung

Die Seite wird über den GitLab-Runner bzw. die `.gitlab-ci.yml` online gestellt. Zur eigenen Nachnutzung muss hier gegebenenfalls angepasst werden. Die HTML-Seiten im `public`-Ordner können lokal ohne Nutzung eines Runners oder einer CI geöffnet werden.

Die Skripte können nach Pull bzw. Download von GitLab lokal über RStudio als Webseite gebaut werden. Hierzu kann nach Öffnen der Datei `Open-Access-Reporting.Rproj` in RStudio der Button "Build Website" genutzt werden. Die benötigten Packages (`tidyverse`, `highcharter`, `DT`, `rmarkdown`) können über die IDE RStudio oder die Kommandozeile installiert werden.

# Lizenz

Dieses Projekt steht unter der MIT-Lizenz zur Verfügung.